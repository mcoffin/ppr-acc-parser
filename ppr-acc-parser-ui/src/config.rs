use std::{
    fs,
    io,
    path::{
        Path,
        PathBuf,
    },
};
pub struct Config {
    directory: PathBuf
}

impl Config {
    #[inline(never)]
    pub fn new() -> Config {
        Config {
            directory: PathBuf::from(".."),
        }
    }

    #[inline(always)]
    pub fn directory(&self) -> &Path {
        &self.directory
    }

    pub fn race_types(&self) -> io::Result<impl Iterator<Item=String>> {
        let entries = fs::read_dir(self.directory())?;
        let ret = entries
            .filter_map(|e| e.ok())
            .filter(|e| {
                let name = e.file_name();
                let name = name.to_str().unwrap();
                for &s in &[".yml", ".yaml"] {
                    if let Some(pos) = name.rfind(s) {
                        if pos == name.len() - s.len() {
                            return true;
                        }
                    }
                }
                false
            })
            .map(|e| {
                let mut filename = e.file_name().to_str().map(|s| s.to_string()).unwrap();
                let last_dot_idx = filename.rfind('.').unwrap();
                if last_dot_idx <= 0 {
                    panic!("invalid filename");
                }
                filename.truncate(last_dot_idx);
                filename
            });
        Ok(ret)
    }
}
