#[macro_use] extern crate clap;
extern crate gtk;
extern crate gio;
extern crate glib;
extern crate log;
extern crate env_logger;

pub mod config;
pub mod session_chooser;

use config::Config;
use std::{
    env,
    io,
    process::Command,
    rc::Rc,
};
use gtk::{
    ContainerExt,
    WidgetExt,
    GtkWindowExt,
    prelude::*,
};
use gio::{
    ApplicationExt,
    prelude::*,
};
use log::{
    debug,
};
use session_chooser::SessionChooser;

const CRATE_SEPARATOR: char = '_';

fn app_id() -> String {
    let mut crate_name = crate_name!().to_string();
    while let Some(idx) = crate_name.find(CRATE_SEPARATOR) {
        crate_name.remove(idx);
    }
    format!("org.{}.ui", &crate_name)
}

trait ConfigExt {
    fn race_types_box(&self) -> io::Result<gtk::ComboBoxText>;
}

impl ConfigExt for Config {
    fn race_types_box(&self) -> io::Result<gtk::ComboBoxText> {
        let race_types = self.race_types()?;
        let cb = gtk::ComboBoxText::new();
        race_types
            .map(|race_type| {
                println!("Found race type: {}", race_type);
                race_type
            })
            .for_each(|race_type| {
                cb.append_text(&*race_type)
            });
        Ok(cb)
    }
}

fn init_ui(app: &gtk::Application, config: Rc<Config>) {
    let win = gtk::ApplicationWindow::new(app);
    win.set_default_size(1280, 720);
    win.set_title("PPR ACC Results Parser");

    let main_box = gtk::Box::new(gtk::Orientation::Vertical, 0);

    let header_bar = gtk::HeaderBarBuilder::new()
        .title("PPR ACC Results Parser")
        .show_close_button(true)
        .build();

    let middle_box = gtk::Box::new(gtk::Orientation::Horizontal, 5);
    let q_chooser = Rc::new(SessionChooser::new("Qualifying Sessions", app));
    let r_chooser = Rc::new(SessionChooser::new("Race Sessions", app));
    middle_box.add(q_chooser.container());
    middle_box.add(r_chooser.container());

    let bottom_bar = gtk::Box::new(gtk::Orientation::Vertical, 5);
    let race_types_box = config.race_types_box()
        .expect("Failed to create race types box");
    let race_types_container = {
        let container = gtk::Box::new(gtk::Orientation::Horizontal, 5);
        let l = gtk::Label::new(Some("Race Type"));
        container.pack_start(&l, false, false, 5);
        container.pack_end(&race_types_box, true, true, 5);
        container
    };
    let generate_button = gtk::Button::new_with_label("Generate");
    {
        let q_chooser = q_chooser.clone();
        let r_chooser = r_chooser.clone();
        let race_types_box = race_types_box.downgrade();
        generate_button.connect_clicked(move |_| {
            let q_sessions = q_chooser.sessions();
            let r_sessions = r_chooser.sessions();
            debug!("Qualifying sessions: {:?}", &q_sessions);
            debug!("Race sessions: {:?}", &r_sessions);
            let mut cmd = Command::new("node");
            cmd.arg("src/index.js");
            cmd.current_dir(".."); // TODO: be smarter about this?
            for s in &q_sessions {
                cmd.arg("-q");
                cmd.arg(s);
            }
            for s in &r_sessions {
                cmd.arg("-r");
                cmd.arg(s);
            }
            let race_types_box = race_types_box.upgrade();
            if let Some(rt) = race_types_box.and_then(|b| b.get_active_id()) {
                cmd.arg("-t");
                cmd.arg(&rt);
            }
            let output = cmd.output().expect("Failed to run ppr-acc-parser");
            if output.status.success() {
                debug!("Generate: success");
            } else {
                debug!("Generate: failure");
            }
        });
    }
    bottom_bar.add(&race_types_container);
    bottom_bar.add(&generate_button);

    main_box.pack_start(&header_bar, false, false, 0);
    main_box.pack_start(&middle_box, true, true, 0);
    main_box.pack_end(&bottom_bar, false, false, 0);

    win.add(&main_box);

    win.show_all();
}

fn main() {
    env_logger::init();
    let config = Rc::new(Config::new());
    let g_app = gtk::Application::new(Some(&app_id()), gio::ApplicationFlags::FLAGS_NONE)
        .expect("Failed to create GTK application");
    {
        let config = config.clone();
        g_app.connect_activate(move |app| {
            init_ui(app, config.clone());
        });
    }
    g_app.run(&env::args().collect::<Vec<_>>());
}
