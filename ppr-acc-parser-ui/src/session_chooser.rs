use gtk::{
    IconSize,
    Orientation,
    prelude::*,
};
use gio::prelude::*;
use std::{
    path::PathBuf,
    sync::{
        Arc,
        RwLock,
    },
};

trait ResponseTypeExt {
    fn is_continue(&self) -> bool;
}

impl ResponseTypeExt for gtk::ResponseType {
    fn is_continue(&self) -> bool {
        use gtk::ResponseType;
        match *self {
            ResponseType::Ok => true,
            ResponseType::Apply => true,
            ResponseType::Accept => true,
            ResponseType::Yes => true,
            _ => false,
        }
    }
}

pub struct SessionChooser {
    container: gtk::Box,
    sessions: gio::ListStore,
    paths: Arc<RwLock<Vec<PathBuf>>>,
}

impl SessionChooser {
    pub fn new<S: AsRef<str>>(chooser_label: S, application: &gtk::Application) -> Self {
        let container = gtk::Box::new(Orientation::Vertical, 0);
        let button_bar = gtk::Box::new(Orientation::Horizontal, 5);
        let sessions = gio::ListStore::new(gtk::Label::static_type());
        let add_button = gtk::Button::new_from_icon_name(Some("list-add-symbolic"), IconSize::Menu);
        let paths = Arc::new(RwLock::new(Vec::with_capacity(2)));
        {
            let chooser_label = String::from(chooser_label.as_ref());
            let application = application.downgrade();
            let sessions = sessions.clone();
            let paths = paths.clone();
            add_button.connect_clicked(move |_| {
                let parent_window = application.upgrade()
                    .and_then(|application| application.get_windows().into_iter().next());
                let file_dialog = gtk::FileChooserDialog::new::<gtk::Window>(
                    Some(&format!("Choose {}", &chooser_label)),
                    parent_window.as_ref(),
                    gtk::FileChooserAction::Open
                );
                file_dialog.set_action(gtk::FileChooserAction::Open);
                file_dialog.add_buttons(&[("Open", gtk::ResponseType::Ok)]);
                let fd = file_dialog.downgrade();
                let sessions = sessions.downgrade();
                let paths = paths.clone();
                file_dialog.connect_response(move |dialog, response_type| {
                    if !response_type.is_continue() {
                        return;
                    }
                    if let Some(fd) = fd.upgrade() {
                        if let Some(sessions) = sessions.upgrade() {
                            fd.get_filenames().iter()
                                .map(|s| s.to_string_lossy())
                                .map(|s| {
                                    let l = gtk::Label::new(Some(s.as_ref()));
                                    (s, l)
                                })
                                .for_each(|(s, l)| {
                                    sessions.append(&l);
                                    let mut paths = paths.write().unwrap();
                                    paths.push(PathBuf::from(s.as_ref()));
                                });
                        }
                    }
                    dialog.destroy();
                });
                file_dialog.show_all();
            });
        }
        let title_label = gtk::Label::new(Some(chooser_label.as_ref()));
        button_bar.pack_start(&title_label, false, false, 5);
        button_bar.pack_end(&add_button, false, false, 5);
        let listbox = gtk::ListBox::new();
        listbox.bind_model(Some(&sessions), move |item| {
            let l = item.downcast_ref::<gtk::Label>().expect("Row data is of wrong type");
            let b = gtk::Box::new(Orientation::Horizontal, 5);
            let remove_button = gtk::Button::new_from_icon_name(Some("list-remove-symbolic"), IconSize::Menu);
            b.pack_start(l, false, false, 5);
            b.pack_end(&remove_button, false, false, 5);
            b.show_all();
            b.clone().upcast::<gtk::Widget>()
        });
        container.pack_start(&button_bar, false, false, 0);
        container.pack_end(&listbox, true, true, 0);
        SessionChooser {
            container: container,
            sessions: sessions,
            paths: paths,
        }
    }

    #[inline(always)]
    pub fn container(&self) -> &gtk::Box {
        &self.container
    }

    pub fn sessions(&self) -> Vec<PathBuf> {
        let sessions = self.paths.read().unwrap();
        sessions.clone()
    }
}
