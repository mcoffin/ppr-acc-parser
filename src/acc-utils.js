const { Transform } = require('stream');

function bufferReplace(buf, before, after) {
    let idx = buf.indexOf(before);
    while (idx !== -1) {
        const prefix = buf.slice(0, idx);
        const suffix = buf.slice(idx + 1, buf.length);
        buf = Buffer.concat([prefix, after, suffix]);
        idx = buf.indexOf(before);
    }
    return buf;
}

class RemoveCharacterFilter extends Transform {
    constructor() {
        super();
        this.filterBuffer = Buffer.from([0]);
    }
    _transform(chunk, encoding, next) {
        if (chunk) {
            const newChunk = bufferReplace(chunk, this.filterBuffer, Buffer.from('', 'utf8'));
            return next(null, newChunk);
        }
        return next();
    }
}

exports.RemoveCharacterFilter = RemoveCharacterFilter;
