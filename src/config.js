const path = require('path');
const yaml = require('yaml');
const _ = require('lodash');
const fs = require('fs');
class Config {
    constructor(options) {
        this.options = options;
    }

    getFiles(mode) {
        if (!this.options[mode]) {
            return [];
        }
        if (Array.isArray(this.options[mode])) {
            return this.options[mode];
        } else {
            return [this.options[mode]];
        }
    }

    get raceFiles() {
        return this.getFiles('race');
    }

    get qualifyingFiles() {
        return this.getFiles('qualifying');
    }

    get type() {
        if (!this.options['type']) {
            return 'classic';
        } else {
            return this.options['type'];
        }
    }

    get pointsMapping() {
        if (_.isUndefined(this._pointsMapping)) {
            const pointsFilename = path.join(__dirname, '..', `${this.type}.yml`);
            this._pointsMapping = yaml.parse(fs.readFileSync(pointsFilename, 'utf8'));
        }
        return this._pointsMapping;
    }

    pointsForPosition(index) {
        if (index >= this.pointsMapping.positions.length) {
            return 0;
        } else {
            return this.pointsMapping.positions[index];
        }
    }

    get(k, d = undefined) {
        return _.get(this.options, k, d);
    }
}
module.exports = Config;
