const { Transform } = require('stream');
const _ = require('lodash');
const Lazy = require('lazy.js');

class CsvTransform extends Transform {
    constructor(mappings) {
        super({
            writableObjectMode: true,
            readableObjectMode: false,
        });
        this.mappings = mappings;
        this.first = true;
    }

    get labels() {
        return Object.keys(this.mappings);
    }

    _transform(chunk, encoding, next) {
        if (!chunk) {
            return next();
        }
        let buffer = null;
        if (this.first) {
            buffer = Buffer.from(this.labels.join(',') + "\n", 'utf8');
            this.first = false;
        }

        const values = Lazy(this.labels).map(k => this.mappings[k]).map(k => {
            let value = _.get(chunk, k, '');
            if (typeof(value) !== 'string') {
                value = `${value}`;
            }
            return value;
        }).toArray();
        const valuesBuffer = Buffer.from(values.join(',') + "\n");
        if (buffer) {
            buffer = Buffer.concat([buffer, valuesBuffer]);
        } else {
            buffer = valuesBuffer;
        }
        return next(null, buffer, 'utf8');
    }
}

exports.CsvTransform = CsvTransform;
