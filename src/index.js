const Lazy = require('lazy.js');
const minimist = require('minimist');
const Config = require('./config');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const { RemoveCharacterFilter } = require('./acc-utils');
const { CsvTransform } = require('./csv-utils');
const options = minimist(process.argv.slice(2), {
    'alias': {
        'race': ['r'],
        'qualifying': ['q'],
        'type': ['t'],
        'output': ['o']
    },
    'string': ['race', 'qualifying', 'type', 'output'],
    'default': {
        'output': process.cwd(),
    }
});
const config = new Config(options);

function readFullStream(stream, encoding = 'utf8') {
    let buf = Buffer.from('', 'utf8');
    return new Promise((resolve, reject) => {
        stream.on('data', (chunk) => {
            buf = Buffer.concat([buf, chunk]);
        });
        stream.on('end', () => resolve(buf));
        stream.on('error', reject);
    });
}

function sideEffectLog(v) {
    console.log(v);
    return v;
}

const racePromises = Lazy(config.raceFiles)
    .map(filename => fs.createReadStream(filename))
    .map(stream => stream.pipe(new RemoveCharacterFilter()))
    .map(readFullStream)
    .map(p => p.then(b => b.toString('utf8')).then(JSON.parse))
    .toArray();

const qualifyingPromises = Lazy(config.qualifyingFiles)
    .map(filename => fs.createReadStream(filename))
    .map(stream => stream.pipe(new RemoveCharacterFilter()))
    .map(readFullStream)
    .map(p => p.then(b => b.toString('utf8')).then(JSON.parse))
    .toArray();

const donePromises = Lazy(_.zip(racePromises, qualifyingPromises))
    .map(([rp, qp]) => qp.then(q => rp.then(r => [r, q])))
    .map((p, raceIndex) => p.then(([data, qData]) => {
        let positionIndex = 0;
        const sessionResults = _.get(data, 'sessionResult.leaderBoardLines', []).map(result => {
            const driver = result.car.drivers[0];
            const driverName = `${driver.firstName} ${driver.lastName}`;
            const qResult = _.find(_.get(qData, 'sessionResult.leaderBoardLines', []), v => _.get(v, 'car.drivers.0.playerId') === driver.playerId);
            const ret = {
                name: driverName,
                playerId: driver.playerId,
                position: positionIndex + 1,
                lapCount: _.get(result, 'timing.lapCount', 0),
                time: _.get(result, 'timing.totalTime', 0),
                points: config.pointsForPosition(positionIndex),
                bestLap: _.get(result, 'timing.bestLap', 0)
            };
            if (qResult) {
                ret.bestQualifyingLap = _.get(qResult, 'timing.bestLap', 0);
            }
            positionIndex++;
            return ret;
        });

        // Apply best lap points
        const bestLapDriver = _.minBy(sessionResults, _.property('bestLap'));
        bestLapDriver.points += config.pointsMapping.fastestLap;

        // Apply pole position points
        const poleDriverId = _.get(_.minBy(_.get(qData, 'sessionResult.leaderBoardLines', []), _.property('timing.bestLap')), 'car.drivers.0.playerId');
        const poleDriver = _.find(sessionResults, res => res.playerId === poleDriverId);
        if (poleDriver) {
            poleDriver.points += config.pointsMapping.pole;
        } else {
            console.error('No pole driver found!');
        }

        return sessionResults;
    }))
    .map((p, raceIndex) => p.then(data => {
        const outputFilename = path.join(config.get('output'), `session-${raceIndex + 1}.csv`);
        const fileStream = fs.createWriteStream(outputFilename);
        const outputStream = new CsvTransform({
            'Name': 'name',
            'Player ID': 'playerId',
            'Position': 'position',
            'Laps': 'lapCount',
            'Time': 'time',
            'Points': 'points',
            'Fastest Lap': 'bestLap',
            'Fastest Qualifying Lap': 'bestQualifyingLap'
        });
        outputStream.pipe(fileStream);
        return new Promise((resolve, reject) => {
            outputStream.on('end', () => resolve(outputFilename));
            outputStream.on('error', reject);
            data.forEach(v => outputStream.write(v));
            outputStream.end();
        });
    }))
    .toArray();
Promise.all(donePromises);
// Promise.all(combinedPromises).then(([raceData, qData]) => {
//     const v = Lazy(raceData).map((data, raceIndex) => {
// 
//     }).toArray();
//     console.log(v);
// });
